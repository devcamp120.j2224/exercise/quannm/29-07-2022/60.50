package com.devcamp.j6040.j6040.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j6040.j6040.model.CCarType;

public interface ICarTypeReposiroty extends JpaRepository<CCarType, Long> {
    
}
