package com.devcamp.j6040.j6040.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j6040.j6040.model.CCar;

public interface ICarReposiroty extends JpaRepository<CCar, Long> {
    CCar findByCarCode(String carCode);
}
